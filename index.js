const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

var admin = require("firebase-admin"); 
//var serviceAccount = require("angularg13-firebase-adminsdk-ngjaw-93e0a5687f.json"); 
admin.initializeApp({
  credential: admin.credential.cert("firebase/angularg13-firebase-adminsdk-ngjaw-93e0a5687f.json"),
  databaseURL: "https://angularg13.firebaseio.com"
});

var database = admin.database();

io.sockets.on('connection', function(socket) {
    socket.on('join', function(data) { 
        socket.username = data.username;
        socket.displayName = data.displayName;
        socket.roomId = data.room; 
        socket.join(data.room);  
        
        var refChats = database.ref("/chats");
        var roomRef = refChats.child(data.room);
        

        let dataResponse = {
            msg : '🔵 Bienvenido ' + data.displayName,
            username: data.username,
            data: [],
            success: true
        }
 
        roomRef.once("value", (snapshot) => {  
            if( !snapshot.exists() ){    
                roomRef.set({ 
                    _id: data.room,
                    msg: [
                        { 
                            text: data.displayName + ' se ha unido al chat',
                            username: data.username,
                            date: new Date().getTime()
                        }
                    ]
                });
                
                io.to(data.room).emit('is_online', dataResponse);
            }else{   
                var msgRef = roomRef.child('msg'); 
                msgRef.push({   
                    text: data.displayName + ' se ha unido al chat.',
                    username: data.username,
                    date: new Date().getTime() 
                }); 

                msgRef.orderByChild("date").limitToLast(10).once("value", (resultDataMsg) => { 
                    dataResponse.data = resultDataMsg.val(); 
                    io.to(data.room).emit('is_online', dataResponse);
                })  
            }
        });
 
    }); 

    socket.on('disconnect', function(data) {     
        console.log(data); 
        io.to(data.room).emit('is_offline', {});
    })

    socket.on('logout', function(data) {   
        var refChats = database.ref("/chats");
        var roomRef = refChats.child(data.room); 
        var msgRef = roomRef.child('msg');

        var dataMsg = {
            text: data.displayName + ' ha salido del chat.',
            username: data.username,
            image: data.image,
            date: new Date().getTime()
        }; 

        msgRef.push(dataMsg);   
        var msgId = msgRef.key;  
        
        var dataResponse = {
            msg: dataMsg, 
            success: (msgId !== null) ? true : false
        }   
    
        io.to(data.room).emit('logout', dataResponse);
    })

    socket.on('chat_message', function(data) {  

        var refChats = database.ref("/chats");
        var roomRef = refChats.child(data.room); 
        var msgRef = roomRef.child('msg');

        var dataMsg = {
            text: data.msg, 
            username: data.username,
            image: data.image,
            date: new Date().getTime()
        }; 

        msgRef.push(dataMsg);   
        var msgId = msgRef.key; 


        var dataResponse = {
            msg: dataMsg, 
            success: (msgId !== null) ? true : false
        } 
        io.emit('chat_message', dataResponse);
    });

});

const server = http.listen(8080, function() {
    console.log('listening on *:8080');
});